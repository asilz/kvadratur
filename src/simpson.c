#include "simpson.h"


//#define M_PI 3.14159265358979323846
//#define M_E  2.71828182845904523536028747135266249775724709369995

double f(double x){
    //printf("%lf",exp(-(x*x)/(2)));
    return exp(-(x*x)/(2.0));
}

/*
double simpson38(double a, double b){
    return ((b-a)/8.0)*(f(a)+3.0*f((2.0*a+b)/(3.0))+3.0*f((a+2.0*b)/(3.0))+f(b));
}

double simpson13(double a, double b){
    //printf("%Lf", b-a);
    return ((b-a)/6.0)*(f(a)+4.0*f((a+b)/(2.0))+f(b));
}



double simpson5(double a, double b){
    return (b - a)/1440.0*(427.0*f(a) + 2724.0*f((2*a + b)/3.0) + 2616.0*f((a + b)/2.0) + 2724.0*f((a + 2*b)/3.0) + 427.0*f(b));
}

*/

double simpson4(double a, double b){
    return (b - a)/90.0*(7.0*f(a) + 32.0*f((3.0*a + b)/4.0) + 12.0*f((a + b)/2.0) + 32.0*f((a + 3.0*b)/4.0) + 7.0*f(b));
}

