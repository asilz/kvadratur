	.file	"simpson.c"
# GNU C17 (Ubuntu 11.3.0-1ubuntu1~22.04) version 11.3.0 (x86_64-linux-gnu)
#	compiled by GNU C version 11.3.0, GMP version 6.2.1, MPFR version 4.1.0, MPC version 1.2.1, isl version isl-0.24-GMP

# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed: -mtune=generic -march=x86-64 -O2 -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection
	.text
	.p2align 4
	.globl	normalDist
	.type	normalDist, @function
normalDist:
.LFB23:
	.cfi_startproc
	endbr64	
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	%xmm0, %xmm0	# tmp92, tmp87
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp88
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp90
	jmp	exp@PLT	#
	.cfi_endproc
.LFE23:
	.size	normalDist, .-normalDist
	.p2align 4
	.globl	simpson38
	.type	simpson38, @function
simpson38:
.LFB24:
	.cfi_startproc
	endbr64	
	movapd	%xmm0, %xmm2	# tmp151, a
	subq	$56, %rsp	#,
	.cfi_def_cfa_offset 64
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	%xmm0, %xmm0	# a, tmp114
# simpson.c:13: double simpson38(double a, double b){
	movsd	%xmm1, 8(%rsp)	# tmp152, %sfp
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp115
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	%xmm2, 16(%rsp)	# a, %sfp
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp117
	call	exp@PLT	#
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	16(%rsp), %xmm2	# %sfp, a
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	%xmm0, 24(%rsp)	# tmp153, %sfp
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	8(%rsp), %xmm0	# %sfp, tmp119
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movapd	%xmm2, %xmm6	# a, tmp119
	addsd	%xmm2, %xmm6	# a, tmp119
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	addsd	%xmm6, %xmm0	# tmp119, tmp119
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	divsd	.LC2(%rip), %xmm0	#, _5
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	%xmm0, %xmm0	# _5, tmp122
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp123
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp125
	call	exp@PLT	#
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	8(%rsp), %xmm5	# %sfp, b
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	16(%rsp), %xmm2	# %sfp, a
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	%xmm0, 40(%rsp)	# _29, %sfp
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movapd	%xmm5, %xmm4	# b, tmp127
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	%xmm2, 32(%rsp)	# a, %sfp
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	addsd	%xmm5, %xmm4	# b, tmp127
	movapd	%xmm4, %xmm0	# tmp127, tmp127
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	addsd	%xmm2, %xmm0	# a, tmp128
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	divsd	.LC2(%rip), %xmm0	#, _10
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	%xmm0, %xmm0	# _10, tmp130
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp131
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp133
	call	exp@PLT	#
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	8(%rsp), %xmm1	# %sfp, b
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	%xmm0, 16(%rsp)	# tmp155, %sfp
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movapd	%xmm1, %xmm5	# b, tmp135
	mulsd	%xmm1, %xmm5	# b, tmp135
	movapd	%xmm5, %xmm0	# tmp135, tmp135
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp136
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp138
	call	exp@PLT	#
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	40(%rsp), %xmm1	# %sfp, _29
	movsd	.LC2(%rip), %xmm7	#, tmp141
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	.LC2(%rip), %xmm6	#, tmp144
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movapd	%xmm0, %xmm3	#, tmp156
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	24(%rsp), %xmm0	# %sfp, tmp141
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	mulsd	%xmm1, %xmm7	# _29, tmp141
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	movsd	32(%rsp), %xmm2	# %sfp, a
	movsd	8(%rsp), %xmm1	# %sfp, b
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	mulsd	16(%rsp), %xmm6	# %sfp, tmp144
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	subsd	%xmm2, %xmm1	# a, b
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	mulsd	.LC3(%rip), %xmm1	#, tmp149
# simpson.c:15: }
	addq	$56, %rsp	#,
	.cfi_def_cfa_offset 8
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	addsd	%xmm7, %xmm0	# tmp141, tmp141
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	addsd	%xmm6, %xmm0	# tmp144, tmp146
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	addsd	%xmm3, %xmm0	# tmp156, tmp147
# simpson.c:14:     return ((b-a)/8.0)*(normalDist(a)+3.0*normalDist((2.0*a+b)/(3.0))+3.0*normalDist((a+2.0*b)/(3.0))+normalDist(b));
	mulsd	%xmm1, %xmm0	# tmp149, tmp140
# simpson.c:15: }
	ret	
	.cfi_endproc
.LFE24:
	.size	simpson38, .-simpson38
	.p2align 4
	.globl	simpson13
	.type	simpson13, @function
simpson13:
.LFB25:
	.cfi_startproc
	endbr64	
	movapd	%xmm0, %xmm2	# tmp129, a
	subq	$40, %rsp	#,
	.cfi_def_cfa_offset 48
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	%xmm0, %xmm0	# a, tmp104
# simpson.c:17: double simpson13(double a, double b){
	movsd	%xmm1, (%rsp)	# tmp130, %sfp
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp105
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	%xmm2, 16(%rsp)	# a, %sfp
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp107
	call	exp@PLT	#
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	movsd	16(%rsp), %xmm2	# %sfp, a
	movsd	(%rsp), %xmm4	# %sfp, tmp109
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	%xmm0, 8(%rsp)	# tmp131, %sfp
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	movsd	.LC1(%rip), %xmm0	#, tmp109
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	addsd	%xmm2, %xmm4	# a, tmp109
	movsd	%xmm2, 24(%rsp)	# a, %sfp
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	mulsd	%xmm4, %xmm0	# tmp109, tmp109
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	%xmm0, %xmm0	# _4, tmp111
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp112
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp114
	call	exp@PLT	#
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	(%rsp), %xmm4	# %sfp, b
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movsd	%xmm0, 16(%rsp)	# _19, %sfp
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movapd	%xmm4, %xmm5	# b, tmp116
	mulsd	%xmm4, %xmm5	# b, tmp116
	movapd	%xmm5, %xmm0	# tmp116, tmp116
# simpson.c:9:     return exp(-(x*x)/(2.0));
	xorpd	.LC0(%rip), %xmm0	#, tmp117
# simpson.c:9:     return exp(-(x*x)/(2.0));
	mulsd	.LC1(%rip), %xmm0	#, tmp119
	call	exp@PLT	#
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	movsd	16(%rsp), %xmm1	# %sfp, _19
	movsd	.LC4(%rip), %xmm6	#, _19
# simpson.c:9:     return exp(-(x*x)/(2.0));
	movapd	%xmm0, %xmm3	#, tmp133
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	movsd	8(%rsp), %xmm0	# %sfp, tmp122
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	movsd	24(%rsp), %xmm2	# %sfp, a
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	mulsd	%xmm1, %xmm6	# _19, _19
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	movsd	(%rsp), %xmm1	# %sfp, b
	subsd	%xmm2, %xmm1	# a, b
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	divsd	.LC5(%rip), %xmm1	#, tmp127
# simpson.c:20: }
	addq	$40, %rsp	#,
	.cfi_def_cfa_offset 8
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	addsd	%xmm6, %xmm0	# _19, tmp122
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	addsd	%xmm3, %xmm0	# tmp133, tmp125
# simpson.c:19:     return ((b-a)/6.0)*(normalDist(a)+4.0*normalDist((a+b)/(2.0))+normalDist(b));
	mulsd	%xmm1, %xmm0	# tmp127, tmp121
# simpson.c:20: }
	ret	
	.cfi_endproc
.LFE25:
	.size	simpson13, .-simpson13
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1071644672
	.align 8
.LC2:
	.long	0
	.long	1074266112
	.align 8
.LC3:
	.long	0
	.long	1069547520
	.align 8
.LC4:
	.long	0
	.long	1074790400
	.align 8
.LC5:
	.long	0
	.long	1075314688
	.ident	"GCC: (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	1f - 0f
	.long	4f - 1f
	.long	5
0:
	.string	"GNU"
1:
	.align 8
	.long	0xc0000002
	.long	3f - 2f
2:
	.long	0x3
3:
	.align 8
4:
