	.file	"main.c"
# GNU C17 (Ubuntu 11.3.0-1ubuntu1~22.04) version 11.3.0 (x86_64-linux-gnu)
#	compiled by GNU C version 11.3.0, GMP version 6.2.1, MPFR version 4.1.0, MPC version 1.2.1, isl version isl-0.24-GMP

# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed: -mtune=generic -march=x86-64 -O2 -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"it took %.16lf to calculate difference %.16lf"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB23:
	.cfi_startproc
	endbr64	
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
# main.c:21:     for(int i = 0; i<n; i++){
	xorl	%ebx, %ebx	# _1
# main.c:13: int main(){
	subq	$24, %rsp	#,
	.cfi_def_cfa_offset 48
# main.c:15:     t = clock();
	call	clock@PLT	#
# main.c:20:     double area = 0;
	movq	$0x000000000, 8(%rsp)	#, %sfp
# main.c:15:     t = clock();
	movq	%rax, %rbp	# tmp121, t
	.p2align 4,,10
	.p2align 3
.L2:
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	pxor	%xmm1, %xmm1	# tmp100
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	pxor	%xmm0, %xmm0	# tmp105
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	pxor	%xmm2, %xmm2	# tmp125
	movl	%ebx, %eax	# _1, i
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	addl	$1, %ebx	#, _1
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	cvtsi2sdl	%eax, %xmm0	# i, tmp105
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	cvtsi2sdl	%ebx, %xmm1	# _1, tmp100
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	mulsd	.LC1(%rip), %xmm0	#, tmp106
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	mulsd	.LC1(%rip), %xmm1	#, tmp101
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	addsd	%xmm2, %xmm0	# tmp125, tmp108
	addsd	%xmm2, %xmm1	# tmp126,
	call	simpson13@PLT	#
# main.c:22:         area += simpson13(a+(step*i), a+(step*(i+1)));
	addsd	8(%rsp), %xmm0	# %sfp, tmp122
	movsd	%xmm0, 8(%rsp)	# tmp122, %sfp
# main.c:21:     for(int i = 0; i<n; i++){
	cmpl	$100000000, %ebx	#, _1
	jne	.L2	#,
# main.c:25:     t = clock() - t;
	call	clock@PLT	#
# main.c:26:     printf("it took %.16lf to calculate difference %.16lf", ((float)t)/CLOCKS_PER_SEC, answer-area);
	pxor	%xmm0, %xmm0	# tmp113
# /usr/include/x86_64-linux-gnu/bits/stdio2.h:112:   return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
	movl	$1, %edi	#,
	movsd	.LC2(%rip), %xmm1	#, tmp111
	subsd	8(%rsp), %xmm1	# %sfp,
# main.c:25:     t = clock() - t;
	subq	%rbp, %rax	# t, t
# /usr/include/x86_64-linux-gnu/bits/stdio2.h:112:   return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
	leaq	.LC4(%rip), %rsi	#, tmp117
# main.c:26:     printf("it took %.16lf to calculate difference %.16lf", ((float)t)/CLOCKS_PER_SEC, answer-area);
	cvtsi2ssq	%rax, %xmm0	# t, tmp113
# /usr/include/x86_64-linux-gnu/bits/stdio2.h:112:   return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
	movl	$2, %eax	#,
# main.c:26:     printf("it took %.16lf to calculate difference %.16lf", ((float)t)/CLOCKS_PER_SEC, answer-area);
	divss	.LC3(%rip), %xmm0	#, tmp114
# main.c:26:     printf("it took %.16lf to calculate difference %.16lf", ((float)t)/CLOCKS_PER_SEC, answer-area);
	cvtss2sd	%xmm0, %xmm0	# tmp114, tmp116
# /usr/include/x86_64-linux-gnu/bits/stdio2.h:112:   return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
	call	__printf_chk@PLT	#
# main.c:29: }
	addq	$24, %rsp	#,
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax	#
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret	
	.cfi_endproc
.LFE23:
	.size	main, .-main
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	-500134854
	.long	1044740494
	.align 8
.LC2:
	.long	1738196034
	.long	1072390470
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC3:
	.long	1232348160
	.ident	"GCC: (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	1f - 0f
	.long	4f - 1f
	.long	5
0:
	.string	"GNU"
1:
	.align 8
	.long	0xc0000002
	.long	3f - 2f
2:
	.long	0x3
3:
	.align 8
4:
