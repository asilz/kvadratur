
#include "simpson.h"

#define a 0.0
#define b 1.0
#define n 1000000
#define answer 0.85562439189214880317
#define step (b-a)/n




int main(){
    clock_t t;
    t = clock();
    //long double a = 0;
    //long double b = 1;
    //long double n = 4;
    //long double step = (b-a)/n;
    double area = 0;
    for(int i = 0; i<n; i++){
        area += simpson4(a+(step*i), a+(step*(i+1)));
        
    }
    t = clock() - t;
    printf("it took %.16lf to calculate difference %.16lf", ((float)t)/CLOCKS_PER_SEC, answer-area);
    return 0;
    
}